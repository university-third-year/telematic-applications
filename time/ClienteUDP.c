#include "ClienteUDP.h"



int sockfd;

void sigint_handlerUDP(int signo)
{
	puts("SIGINT received...");
	exit(0);
}
void UDPClient(int port,char* host, int dep){
struct hostent *server;
 struct sockaddr_in serveraddr;
 int serverlen;

	uint32_t buf;
	int n;
	int msg_len;
	uint32_t seconds;

	/* handler SIGINT signal*/
	signal(SIGINT, sigint_handlerUDP);

	/* socket: create the socket */
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) {
		perror("ERROR opening socket");
		exit(0);
	}
	if(dep==1){printf("Socket creado con identificador: %d\n", sockfd);}

	/* gethostbyname: get the server's DNS entry */
	server = gethostbyname(host);
	if (server == NULL) {
		fprintf(stderr,"ERROR, no such host: %s\n", host);
		exit(0);
	}

	/* build the server's Internet address */
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, server->h_length);
	serveraddr.sin_port = htons(port);
	serverlen=sizeof(serveraddr);
	if(dep==1){printf("Sistema intentará conectar con el servidor %s por el puerto %d\n", host, port);}

		msg_len = sizeof(uint32_t);
		/* send the message line to the server */
		n = sendto(sockfd, &buf, msg_len,0,(struct sockaddr *) &serveraddr, (unsigned int) serverlen);
		if (n < 0) {
			perror("ERROR writing to socket");
			exit(0);
		}
		if(dep==1){printf("Datagrama a 0 enviado.\n");}

		/* read answer from the server */
			n = recvfrom(sockfd, &seconds,sizeof(uint32_t), 0,(struct sockaddr *) &serveraddr,(socklen_t *) &serverlen);

			if (n < 0) {
				perror("ERROR reading from socket");
				exit(0);
			}
			if(dep==1){printf("Respuesta recibida por %s\n", host);}
		uint32_t secondsh=ntohl(seconds);
		uint32_t difference=2208988800;
		uint32_t real_seconds=secondsh-difference;

		//printf("%" PRIu32 "\n",real_seconds);
		char my_time[50];
		struct tm *tmp;
		time_t t= real_seconds;
		tmp=localtime(&t);
		strftime(my_time,sizeof(my_time),"%a %b %d %H:%M:%S %Z %Y",tmp);
		printf("%s\n",my_time);


	close(sockfd);
	if(dep==1){printf("Socket con identificador %d cerrado.\n", sockfd);}
}
