#include "ServerTCP.h"

#define BACKLOG 10	 // how many pending connections queue will hold

void sigchld_handler(int s);
int TCPechod(int fd, int dep);

void TCPServer(int port, int dep){
  //printf("%d\n", port);
  int clientlen; // byte size of client's address
	int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
  struct sockaddr_in serveraddr; // server's addr
  struct sockaddr_in clientaddr; // client addr
  struct hostent *hostp; // client host info
  int optval; // flag value for setsockopt
  int cc;

  /* socket: create the parent socket */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    perror("ERROR opening socket");
    exit(0);
  }
  if(dep==1){printf("Socket creado con identificador: %d\n", sockfd);}

  /* setsockopt: lets s rerun the server immediately after we kill it.
* Eliminates "ERROR on binding: Address already in * use"
* error. */
  optval = 1;
  setsockopt(new_fd, SOL_SOCKET, SO_REUSEADDR,
  (const void *)&optval , sizeof(int));

  /* build the server's Internet address */
  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short)port);

  /* bind: associate the parent socket with a port */
  if (bind(sockfd, (struct sockaddr *) &serveraddr,
    sizeof(serveraddr)) < 0) {
    perror("ERROR on binding");
    exit(0);
  }
  if (dep==1){printf("Socket binded.\n");}
  /* handler SIGCHLD signal */
  signal(SIGCHLD, sigchld_handler);

  while(1) {
  /* listen: make this socket ready to accept connection requests */
  if (listen(sockfd, BACKLOG) < 0) {
    perror("ERROR on listen");
    exit(0);
  }
  if (dep==1){printf("Escuchando en puerto: %d\n", port);}
  /* wait for a connection request */
 // main accept() loop
    clientlen = sizeof(clientaddr);
    new_fd = accept(sockfd, (struct sockaddr *)&clientaddr,&clientlen);
    if (new_fd == -1) {
      perror("ERROR on accept");
      exit(0);
    }
    if(dep==1){printf("Cliente aceptado.\n");}
    /* gethostbyaddr: determine who sent the message */
    hostp = gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,sizeof(clientaddr.sin_addr.s_addr), AF_INET);
    if (hostp == NULL) {
      perror("ERROR on gethostbyaddr");
      exit(0);
    }

    if (!fork()) { // this is the child process
      close(sockfd);
      TCPechod(new_fd, dep);
      close(new_fd);
			exit(0);
		}
		  // parent doesn't need this


  }

}

void sigchld_handler(int s)
{
	while(waitpid(-1, NULL, WNOHANG) > 0);
}

int TCPechod(int fd, int dep){
	uint32_t seconds;
  uint32_t x;
	int	cc;

  cc=recv(fd,&x,sizeof(uint32_t),0);
  uint32_t xnth=ntohl(x);
  if(dep==1){printf("%d recibido del cliente\n",xnth);}
  //printf("%" PRIu32 "\n",xnth);

  for(int i=0;i<5;i++){
  /*obtain the local time*/

  time_t localtime;
  struct tm *tmp;
  time(&localtime);
  seconds=htonl(localtime+2208988800-xnth);

		/* echo the input string back to the client */
    if (send(fd, &seconds, sizeof(uint32_t), 0) < 0){
      fprintf(stderr, "ERROR echo write: %s\n",
			strerror(errno));
      exit(1);
		}
    if (dep==1){printf("Datagrama con información sobre hora y fecha enviado al cliente.\n");}
    sleep(2);
  }
	return 0;

}
