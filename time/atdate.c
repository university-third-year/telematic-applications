#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ClienteUDP.h"
#include "ClienteTCP.h"
#include "ServerTCP.h"

int main(int argc, char **argv){
  char* host="";
  int port=37;
  char* mode="cu";
  int dep=0;
  int x=0;
  for (int i = 0; i < argc; ++i){
    if(argc>10){
      printf("Error. Too many arguments");
      return(0);
    }
    if(argc<3){
      printf("Error. Too few arguments.");
      return(0);
    }
    if((strcmp(argv[i],"-s")==0)&&((i+1)<argc)){
      host=argv[i+1];
    }
    if(strcmp(argv[i],"-p")==0){
      port=atoi(argv[i+1]);
    }
    if(strcmp(argv[i],"-m")==0){
      mode=argv[i+1];
    }
    if(strcmp(argv[i],"-d")==0){
      dep=1;
    }
    if(strcmp(argv[i],"-x")==0){
      x=atoi(argv[i+1]);
    }

if(((i%2)==1)&&(strcmp(argv[i],"-m")!=0)&&(strcmp(argv[i],"-p")!=0)&&(strcmp(argv[i],"-s")!=0)&&(strcmp(argv[i],"-d")!=0)&&(strcmp(argv[i],"-x")!=0)){
  printf("Invalid argument\n");
  return(0);
}
  }
  if((strcmp(mode,"s")!=0)&&(strcmp(mode,"cu")!=0)&&(strcmp(mode,"ct")!=0)){
    printf("Error, mode must be cu, ct or s\n");
    return(0);
  }
  if((port<1024)&&port!=37){
    printf("Error, port must be greater than 1024 or 37(dft)\n");
    return(0);
  }
  if(port>65535){
    printf("Error, port must be smaller than 65535 or 37(dft)\n");
    return(0);
  }
  if(((strcmp(mode,"cu")==0)||(strcmp(mode,"ct")==0))&&(strcmp(host,"")==0)){
    printf("Error, you must add a server\n" );
    return(0);
  }
  if(strcmp(mode,"cu")==0) UDPClient(port,host,dep);
  if(strcmp(mode,"ct")==0) TCPClient(port,host,dep,x);
  if(strcmp(mode,"s")==0) TCPServer(port,dep);
}
